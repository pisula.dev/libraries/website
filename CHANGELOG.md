## [1.7.0] - 2023-01-13
### Added
- Minify HTML

## [1.6.0] - 2022-12-10
### Added
- Eager Loading support

## [1.5.0] - 2022-12-02
### Added
- CPT template column support

## [1.4.2] - 2022-12-02
### Fixed
- disabled seo .htaccess rules on non apache servers

## [1.4.1] - 2022-12-02
### Fixed
- .htaccess rules changes

## [1.4.0] - 2022-11-30
### Added
- updates info
- deflate changes

## [1.3.0] - 2022-10-03
### Added
- support preview block styles

## [1.2.1] - 2022-10-02
### Changed
- better optimization expires

## [1.2.0] - 2022-10-02
### Changed
- restored optimization file

## [1.1.0] - 2022-09-21
### Changed
- sync library

## [1.0.2] - 2022-09-21
### Fixed
- fatal error

## [1.0.1] - 2022-09-21
### Fixed
- fatal error

## [1.0.0] - 2022-09-21
### Added
- initial version
