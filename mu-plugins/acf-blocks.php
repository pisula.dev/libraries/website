<?php

/**
 * Plugin Name: ACF Blocks Manager
 */
class IC_ACF_Blocks {
	/**
	 * IC_ACF_Fields constructor.
	 */
	public function __construct() {
		add_action( 'acf/init', [ $this, 'init' ] );
	}

	/**
	 * .
	 */
	public function init(): void {
		$blocks = wp_get_theme()->get_page_templates( null, 'ic-acf-block' );

		foreach ( $blocks as $file => $name ) {
			if ( $name === 'Example' ) {
				continue;
			}

			$data = get_file_data(
				get_theme_file_path( $file ),
				[
					'category' => 'Block Category',
					'title'    => 'Template Name',
					'icon'     => 'Block Icon',
					'keywords' => 'Block Keywords',
					'preview'  => 'Block Preview',
				],
				'acf_blocks'
			);

			$data['keywords'] = array_filter( wp_parse_list( $data['keywords'] ) );

			$block_name = wp_basename( $file, '.php' );

			$args = array_merge(
				$data,
				[
					'name'            => $block_name,
					'render_callback' => function ( $block, $content, $is_preview, $post_id, $wp_block, $context ) use ( $file ) {
						if ( $block['data']['_preview_url'] ?? false ) {
							echo '<img src="' . esc_url( $block['data']['_preview_url'] ) . '" style="width:100%; height:auto;">';
						} else {
							$args = $block['data'] ?? [];

							include locate_template( $file );
						}
					},
					'enqueue_assets'  => function () use ( $block_name ) {
						if ( ! is_admin() ) {
							return;
						}

						$file = 'assets/dist/blocks/' . $block_name . '.css';

						if ( ! file_exists( get_theme_file_path( $file ) ) ) {
							return;
						}

						wp_enqueue_style( 'block-' . $block_name, get_theme_file_uri( $file ) );
					},
				]
			);

			if ( is_admin() ) {
				$args['example'] = [
					'attributes' => [
						'mode' => 'preview',
						'data' => [
							'_preview_url' => $this->get_preview_url( $data['preview'] ?? '' ),
						],
					],
				];
			}

			acf_register_block_type( $args );
		}
	}

	private function get_preview_url( string $file ): string {
		$files = [];
		$path  = 'assets/img/block-preview/';

		if ( $file ) {
			$files[] = $path . $file;
		}

		$files[] = $path . 'default.png';

		$files = array_values(
			array_filter(
				$files,
				static function ( $file ) {
					return file_exists( get_theme_file_path( $file ) );
				}
			)
		);

		return get_theme_file_uri( $files[0] );
	}
}
