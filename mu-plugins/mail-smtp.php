<?php
/**
 * Plugin Name: SMTP Configuration
 */

/**
 * SMTP Configuration.
 */
class IC_Mail {

	/**
	 * @var array
	 */
	private array $wp_mail = [];

	/**
	 * .
	 */
	public function add_hooks(): void {
		if ( ! $this->get_const_value( 'SMTP_ENABLED', false ) ) {
			return;
		}

		add_filter( 'wp_mail', [ $this, 'wp_mail' ], PHP_INT_MAX );
		add_filter( 'wp_mail_from', [ $this, 'wp_mail_from' ] );
		add_filter( 'wp_mail_from_name', [ $this, 'wp_mail_from_name' ] );
		add_action( 'phpmailer_init', [ $this, 'phpmailer_init' ], PHP_INT_MAX );
		add_action( 'wp_mail_failed', [ $this, 'wp_mail_failed' ] );
	}

	/**
	 * @param mixed $args
	 *
	 * @return array
	 */
	public function wp_mail( $args ): array {
		$this->wp_mail = $args;

		return $args;
	}

	/**
	 * @param WP_Error $error
	 */
	public function wp_mail_failed( WP_Error $error ): void {
		trigger_error( $error->get_error_message() . ': ' . serialize( $error->error_data ) ); // phpcs:ignore
	}

	/**
	 * Fires after PHPMailer is initialized.
	 *
	 * @param PHPMailer $phpmailer The PHPMailer instance (passed by reference).
	 */
	public function phpmailer_init( $phpmailer ): void {
		$phpmailer->IsSMTP();

		$phpmailer->Host       = $this->get_const_value( 'SMTP_HOST' ); // phpcs:ignore
		$phpmailer->SMTPAuth   = $this->get_const_value( 'SMTP_AUTH', true ); // phpcs:ignore
		$phpmailer->Port       = $this->get_const_value( 'SMTP_PORT', 465 ); // phpcs:ignore
		$phpmailer->Username   = $this->get_const_value( 'SMTP_AUTH_LOGIN' ); // phpcs:ignore
		$phpmailer->Password   = $this->get_const_value( 'SMTP_AUTH_PASSWORD' ); // phpcs:ignore
		$phpmailer->SMTPSecure = $this->get_const_value( 'SMTP_SECURE', 'ssl' ); // phpcs:ignore

		$headers = $this->get_headers( $this->wp_mail['headers'] ?? [] );

		if ( ! isset( $headers['Reply-To'] ) ) {
			try {
				$phpmailer->addReplyTo( $this->get_const_value( 'SMTP_REPLY_EMAIL' ), $this->get_const_value( 'SMTP_REPLY_NAME' ) );
			} catch ( \PHPMailer\PHPMailer\Exception $e ) {
				trigger_error( $e->getMessage() ); // phpcs:ignore
			}
		}
	}

	/**
	 * Filters the email address to send from.
	 *
	 * @param mixed $from_email Email address to send from.
	 *
	 * @return string
	 */
	public function wp_mail_from( $from_email ): string {
		return $this->get_const_value( 'SMTP_FROM_EMAIL', $from_email );
	}

	/**
	 * Filters the name to associate with the "from" email address.
	 *
	 * @param mixed $from_name Name associated with the "from" email address.
	 *
	 * @return string
	 */
	public function wp_mail_from_name( $from_name ): string {
		return $this->get_const_value( 'SMTP_FROM_NAME', $from_name );
	}

	/**
	 * @param string $name    .
	 * @param string $default .
	 *
	 * @return mixed
	 */
	private function get_const_value( string $name, string $default = '' ) {
		return defined( $name ) ? constant( $name ) : $default;
	}

	/**
	 * @param string|array $headers .
	 *
	 * @return string[]
	 */
	private function get_headers( $headers ): array {
		if ( is_string( $headers ) ) {
			$headers_array = array_filter( preg_split( "/\r\n|\n|\r/", $headers ) );

			$headers = [];

			foreach ( $headers_array as $header_string ) {
				[ $key, $value ] = explode( ':', $header_string );

				$headers[ $key ] = trim( $value );
			}
		}

		return $headers;
	}
}
