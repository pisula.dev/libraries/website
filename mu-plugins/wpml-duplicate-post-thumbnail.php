<?php
/**
 * Plugin Name: WPML Featured image duplication fix
 * Description: When a post is duplicated the featured image from the source post is copied to the duplicate post
 * Version: 1.0
 * Author: Sebastian Pisula
 * Author URI: mailto:sebastian.pisula@gmail.com
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

class IC_Duplicate_Post_Thumbnail {

	function __construct() {
		add_action( 'icl_make_duplicate', [ $this, 'make_duplicate' ], 99, 4 );
	}

	/**
	 * @param int    $master_post_id .
	 * @param string $lang           .
	 * @param array  $post_array     .
	 * @param int    $id             .
	 */
	public function make_duplicate( $master_post_id, $lang, $post_array, $id ) {
		set_post_thumbnail( $id, get_post_thumbnail_id( $master_post_id ) );
	}
}
