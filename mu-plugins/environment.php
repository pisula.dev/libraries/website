<?php
/**
 * Plugin Name: Environment Configuration
 */

/**
 * Class IC_Environment
 */
class IC_Environment {
	public function __construct() {
		// Disable ACF in admin menu.
		add_filter( 'acf/settings/show_admin', 'ic_is_local' );

		// .Block emails.
		add_filter( 'wp_mail', [ $this, 'wp_mail' ], 1000 );

		if ( ic_is_development() || ( ic_is_local() && WP_DEBUG && WP_DEBUG_DISPLAY ) ) {
			set_error_handler( [ $this, 'error_handler' ] );
		}
	}

	/**
	 * @param int    $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param int    $errline
	 *
	 * @return bool
	 */
	public function error_handler( $errno, $errstr, $errfile, $errline ) {
		if ( ! ( error_reporting() & $errno ) ) {
			return false;
		}

		echo '<div style="top:0;left:0;position: fixed;z-index: 100000;width: 100%;height: 100%;background: red;padding: 80px;text-align: center;color: black;justify-content: center;align-items: center;display: flex;">' . $errstr . ' in ' . $errfile . ':' . $errline . '</div>';

		return true;
	}

	/**
	 * WP Mail args filter
	 *
	 * @param array $args
	 *
	 * @return mixed
	 */
	public function wp_mail( $args ) {
		if ( ic_is_production() ) {
			return $args;
		}

		if ( defined( 'WP_MAIL_TEST' ) && ! empty( WP_MAIL_TEST ) ) {
			$args['to'] = WP_MAIL_TEST;
		}

		$args['subject'] = strtoupper( wp_get_environment_type() ) . ': ' . $args['subject'];

		return $args;
	}
}
