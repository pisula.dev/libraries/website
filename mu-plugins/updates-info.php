<?php

class IC_Updates_Info {

	/**
	 * @return void
	 */
	public function add_hooks(): void {
		add_action( 'load-plugins.php', [ $this, 'add_upgrade_plugin_info' ], 30 );
	}

	/**
	 * @return void
	 */
	public function add_upgrade_plugin_info(): void {
		if ( current_user_can( 'update_plugins' ) ) {
			return;
		}

		add_action( 'after_plugin_row', 'wp_plugin_update_row', 10, 2 );
	}
}
