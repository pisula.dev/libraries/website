<?php

class IC_Log {
	/**
	 * .
	 */
	public function add_hooks() {
		add_action( 'init', [ $this, 'init' ] );
	}

	/**
	 * .
	 */
	public function init() {
		if ( ic_is_production() ) {
			return;
		}

		add_action( 'template_redirect', [ $this, 'auth_admin' ], 0 );
	}

	/**
	 * .
	 */
	public function auth_admin() {
		global $wp;

		if ( $wp->request !== 'log.php' ) {
			return;
		}

		if ( current_user_can( 'administrator' ) ) {
			wp_safe_redirect( admin_url(), 301 );
			die();
		}

		nocache_headers();

		$user_id = $this->get_user_id();

		if ( $user_id > 0 ) {
			wp_set_auth_cookie( $user_id );
			wp_safe_redirect( admin_url(), 301 );
			die();
		}

		wp_die( 'NO ADMIN' );
	}

	/**
	 * @return int
	 */
	private function get_user_id(): int {
		if ( defined( 'AUTH_USER_ID' ) && AUTH_USER_ID > 0 ) {
			return (int) AUTH_USER_ID;
		}

		$wp_user_query = new WP_User_Query(
			[
				'role'    => 'Administrator',
				'number'  => 1,
				'fields'  => 'ID',
				'orderby' => 'registered',
				'order'   => 'ASC',
			]
		);

		return (int) ( $wp_user_query->get_results()[0] ?? 0 );
	}
}
