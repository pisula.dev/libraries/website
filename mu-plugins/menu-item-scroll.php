<?php

class IC_Active_Menu_Item_Scroll {

	public function add_hooks() {
		add_filter( 'nav_menu_link_attributes', [ $this, 'nav_menu_link_attributes' ], 10, 2 );
	}

	/**
	 * @param array   $atts
	 * @param WP_Post $item
	 *
	 * @return array
	 */
	public function nav_menu_link_attributes( $atts, $item ) {
		if ( 'custom' === $item->type && substr( $item->url, 0, 1 ) === '#' ) {
			$atts['href'] = trailingslashit( home_url() ) . $item->url;
		}

		return $atts;
	}
}
