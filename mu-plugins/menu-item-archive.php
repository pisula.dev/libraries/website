<?php

class IC_Active_Menu_Item_Archive {

	public function add_hooks() {
		add_filter( 'nav_menu_css_class', [ $this, 'nav_menu_css_class' ], 10, 2 );
	}

	/**
	 * @param array   $classes
	 * @param WP_Post $item
	 *
	 * @return array
	 */
	public function nav_menu_css_class( $classes, $item ) {
		if ( $this->is_active_item( $item ) ) {
			$classes[] = 'current-menu-item';
		}

		return $classes;
	}

	/**
	 * @param WP_Post $item .
	 *
	 * @return bool
	 */
	private function is_active_item( $item ) {
		return 'post_type_archive' === $item->type && is_singular( $item->object );
	}
}
