<?php

namespace IC\Functionality\Website;

/**
 * .
 */
class EagerLoading {

	/**
	 * @var bool .
	 */
	private bool $used = false;

	private const OPTION_NAME = 'eager_loading';

	/**
	 * @return void
	 */
	public function add_hooks(): void {
		add_action( 'acf/init', [ $this, 'register_fields' ] );
		add_filter( 'wp_get_attachment_image_attributes', [ $this, 'add_loading_eager' ], 10, 2 );
	}

	/**
	 * @return void
	 */
	public function register_fields(): void {
		acf_add_local_field_group(
			[
				'key'                   => 'group_6392275572fba',
				'title'                 => 'Eager Loading',
				'fields'                => [
					[
						'key'               => 'field_639227551ad6a',
						'label'             => 'Eager Loading',
						'name'              => self::OPTION_NAME,
						'aria-label'        => '',
						'type'              => 'true_false',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => [
							'width' => '',
							'class' => '',
							'id'    => '',
						],
						'status'            => 1,
						'message'           => 'Enable the eager loading support for this image',
						'default_value'     => 0,
						'ui'                => 0,
						'ui_on_text'        => '',
						'ui_off_text'       => '',
					],
				],
				'location'              => [
					[
						[
							'param'    => 'attachment',
							'operator' => '==',
							'value'    => 'image',
						],
					],
				],
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => true,
				'description'           => '',
				'show_in_rest'          => 0,
			]
		);
	}

	/**
	 * @param mixed   $attr       .
	 * @param \WP_Post $attachment .
	 *
	 * @return array
	 */
	public function add_loading_eager( $attr, \WP_Post $attachment ): array {
		$attr = is_array( $attr ) ? $attr : [];

		if ( ! $this->should_add_attribute() ) {
			return $attr;
		}

		if ( get_field( self::OPTION_NAME, $attachment ) ) {
			$attr['loading'] = 'eager';

			$this->used = true;
		}

		return $attr;
	}

	/**
	 * @return bool
	 */
	private function should_add_attribute(): bool {
		return function_exists( 'get_field' ) && ! $this->used;
	}
}
