<?php

namespace IC\Functionality\Website;

/**
 * .
 */
class MinifyHTML {
	public function add_hooks(): void {
		if ( defined( 'MINIFY_HTML' ) && MINIFY_HTML === true ) {
			add_action( 'wp_loaded', [ $this, 'run_minifier' ] );
		}
	}

	/**
	 * @return void
	 */
	public function run_minifier(): void {
		if ( ! is_admin() ) {
			ob_start( [ $this, 'minify_html' ] );
		}
	}

	/**
	 * @param string $html .
	 *
	 * @return string
	 */
	public function minify_html( string $html ): string {
		$ignore_tags_regex = implode( '|', [ 'textarea', 'pre', 'code' ] );

		// Remove HTML comments.
		$minified_html = preg_replace( '#<!--[^\[><].*?-->#s', '', $html );

		// Remove CSS and JavaScript comments.
		$minified_html = preg_replace(
			'#/\*(?!!)[\s\S]*?\*/|(?:^[ \t]*)//.*$|((?<!\()[ \t>;,{}[\]])//[^;\n]*$#m',
			'$1',
			$minified_html
		);

		// Replace whitespaces of any size with a single space.
		$minified_html = preg_replace(
			'#(?>[^\S ]\s*|\s{2,})(?=[^<]*+(?:<(?!/?(?:' . $ignore_tags_regex . ')\b)[^<]*+)*+(?:<(?>' . $ignore_tags_regex . ')\b|\z))#ix',
			' ',
			$minified_html
		);

		if ( strlen( $minified_html ) <= 1 ) {
			return $html; // HTML minification failed.
		}

		return $minified_html;
	}
}
