<?php

namespace IC\Functionality\Website;

use IC\Functionality\ACF\Fields\AcfFieldsHooks;
use IC\Functionality\ACF\FlexibleContent\FlexibleContentHooks;
use IC\Functionality\BasicAuth;
use IC\Functionality\RemoteUploads;
use IC_ACF_Blocks;
use IC_Active_Menu_Item_Archive;
use IC_Active_Menu_Item_Scroll;
use IC_Duplicate_Post_Thumbnail;
use IC_Environment;
use IC_Log;
use IC_Mail;
use IC_Optimization;
use IC_Page_Template;
use IC_Security;
use IC_Seo;
use IC_Updates_Info;

class WebsiteFunctionality {
	public function hooks(): void {
		// Modules.
		( new BasicAuth() )->hooks();
		( new AcfFieldsHooks() )->hooks();
		( new FlexibleContentHooks() )->hooks();

		if ( defined( 'WP_REMOTE_URL' ) && ! empty( WP_REMOTE_URL ) ) {
			( new RemoteUploads( WP_REMOTE_URL ) )->hooks();
		}

		// Old functionality.
		new IC_Seo();
		new IC_Security();
		new IC_ACF_Blocks();
		new IC_Environment();
		new IC_Optimization();
		new IC_Duplicate_Post_Thumbnail();
		( new IC_Log() )->add_hooks();
		( new IC_Mail() )->add_hooks();
		( new IC_Page_Template )->add_hooks();
		( new IC_Updates_Info() )->add_hooks();
		( new IC_Active_Menu_Item_Scroll )->add_hooks();
		( new IC_Active_Menu_Item_Archive() )->add_hooks();

		// New functionality.
		( new MinifyHTML() )->add_hooks();
		( new EagerLoading() )->add_hooks();
	}
}
